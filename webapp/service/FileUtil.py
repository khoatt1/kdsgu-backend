import os
import urllib.request
from flask import Flask, request, redirect, jsonify
from werkzeug.utils import secure_filename
import config
import fitz
import base64


class FileUtil:

    def __init__(self):
      self.ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

    def allowedFile(self, filename):
      return '.' in filename and filename.rsplit('.', 1)[1].lower() in self.ALLOWED_EXTENSIONS

    def upload(self, type, filename):  #type:: 0: for MC, 1: for BaoCao
      if 'file' not in request.files:
        resp = jsonify({'message' : 'No file part in the request'})
        resp.status_code = 400
        return resp

      file = request.files['file']
      if file.filename == '':
        resp = jsonify({'message' : 'No file selected for uploading'})
        resp.status_code = 400
        return resp

      if file and self.allowedFile(file.filename):
        filename = secure_filename(filename + os.path.splitext(file.filename)[-1])
        if type==0:
          file.save(os.path.join(config.UPLOAD_FOLDER_PROOF, filename))
        elif type==1:
          file.save(os.path.join(config.UPLOAD_FOLDER_TEMP, filename))
          
        resp = jsonify({'message' : 'File successfully uploaded'})
        resp.status_code = 201
        return resp
      else:
        resp = jsonify({'message' : 'Allowed file types are txt, pdf, png, jpg, jpeg, gif'})
        resp.status_code = 400
        return resp

    def mergeProof(self, ids):
      try:
        result = fitz.open()
        for id in ids:
          if not os.path.isfile(config.UPLOAD_FOLDER_PROOF+"{}.pdf".format(id)):
            misspage = result.new_page()
            where = fitz.Point(50, 150)
            misspage.insertText(where, "Missing: {}.pdf".format(id), fontsize=50, color=(1, 0, 0))
            continue
          with fitz.open(config.UPLOAD_FOLDER_PROOF+"{}.pdf".format(id)) as mfile:
            result.insertPDF(mfile, links=False, annots=False)
        return base64.b64encode(result.tobytes())
      except Exception as e:
        return str(e), False
