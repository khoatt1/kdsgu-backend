
import urllib.request
from werkzeug.utils import secure_filename
from borb.toolkit.text.simple_text_extraction import SimpleTextExtraction
from borb.pdf.document import Document  
from borb.pdf.pdf import PDF  
from borb.toolkit.text.regular_expression_text_extraction import RegularExpressionTextExtraction
from PyPDF4 import PdfFileWriter, PdfFileReader
from models import *
import fitz
import database
import sys
import typing  
import math
import config
import os
import re

class LinkMaker:
  def __init__(self):
    pass

  def getProofURI(self, idmh, idbc):
    enproofs = MaHoaMinhChung.query.filter_by(idmahoa=idmh, idbaocao=idbc).all()
    if len(enproofs)==1:  #check proof is a link
      proof = MinhChung.query.filter_by(mamc=enproofs[0].mamc).first()
      if proof.maloai==0:
        return proof.url
    global uri
    uri = config.PROOF_VIEW_PAGE_URI
    for it in enproofs:
      uri = uri + "/{}".format(it.mamc)
    return uri

  #combine PyPDF4 and borb to extract and locate MHCM
  def tagLink(self, reportID):
    regex = "(?<=\[)(\w+\.\w+.*?)(?=\])"
    
    report = BaoCao.query.filter_by(id=reportID).first()
    reportURI = config.UPLOAD_FOLDER_REPORT + "{}.pdf".format(report.id)

    with open(reportURI, "r+b") as file:
      input = PdfFileReader(file)
      output = PdfFileWriter()
      doc: typing.Optional[Document] = None  
      l0: RegularExpressionTextExtraction = RegularExpressionTextExtraction(regex)  
      l1: SimpleTextExtraction = SimpleTextExtraction()
      doc = PDF.loads(file, [l0, l1]) 
      adjustSize = 2
      assert doc is not None

      for pagenum in range(0, input.getNumPages()):
        page = input.getPage(pagenum)
        output.addPage(page)
        idmhs = re.findall(regex, l1.get_text_for_page(pagenum))
        recs: typing.List[PDFMatch] = l0.get_matches_for_page(pagenum) 
        idx = 0   
        for rec in recs:
          r: Rectangle = rec.get_bounding_boxes()[0]
          output.addURI(pagenum, self.getProofURI(idmhs[idx], report.id),[r.get_x()-adjustSize , r.get_y()-adjustSize, r.get_x()+r.get_width()+adjustSize, r.get_y()+r.get_height()+adjustSize], border=[])
          idx += 1
      output.write(file)


  #Improvement using PyMuPDF with self-built MuPDF
  def tagLink2(self, reportID):
    # regex = "(?<=\[)(\w+\.\w+.*?)(?=\])"
    regex = "([H-H]\d+\.\d+\.\d+\.\d+)"
    try:
      report = BaoCao.query.filter_by(id=reportID).first()
      source = config.UPLOAD_FOLDER_TEMP + "{}.pdf".format(report.id)
      dest = config.UPLOAD_FOLDER_REPORT + "{}.pdf".format(report.id)

      doc = fitz.open(source)
      epsilon = 0.2

      for page in doc:
        idmhs = re.findall(regex, page.get_text())
        for idmh in idmhs:
          rects = page.searchFor(idmh)

          if len(rects)==2 and abs(rects[0].x1-rects[1].x0)<epsilon:
            rects = [self.findBoundingRec(rects[0], rects[1])]

          for i in rects:
            clr = fitz.utils.getColor("py_color")
            # rect = fitz.Rect(i.x0, i.y0, i.x1, i.y1)
            page.draw_rect(i, color=clr, fill=clr, overlay=False)
            link = page.insertLink({'kind':2, 'from': i, 'uri': self.getProofURI(idmh, report.id)})
            # page.drawRect(rect, (0.25, 1, 0.25))

      doc.save(dest, garbage=4, deflate=True, clean=True)

    except Exception as e:
        return str(e), False
    else:
        return "Success", True


  def findBoundingRec(self, rect1, rect2):
    ulx = rect1.x0 if rect1.x0 < rect2.x0 else rect2.x0
    uly = rect1.y0 if rect1.y0 > rect2.y0 else rect2.y0
    lrx = rect1.x1 if rect1.x1 > rect2.x1 else rect2.x1
    lry = rect1.y1 if rect1.y1 > rect2.y1 else rect2.y1
    return fitz.Rect(ulx, uly, lrx, lry)

