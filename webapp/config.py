import os

user = 'test'
password = 'test'
database = 'test'
container_name = 'postgres'
port = 5432

DATABASE_CONNECTION_URI = f'postgresql+psycopg2://{user}:{password}@{container_name}:{port}/{database}'
JWT_SECRET_KEY='daihocSaiGon@273adv'

UPLOAD_FOLDER_PROOF = "./storage/proof/"
UPLOAD_FOLDER_REPORT = "./storage/report/"
UPLOAD_FOLDER_TEMP = "./storage/temp/"

PROOF_VIEW_PAGE_URI = "http://sgu.codes:3000/ViewMC"