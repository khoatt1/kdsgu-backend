from flask import Flask, jsonify, request, send_file, send_from_directory, after_this_request, copy_current_request_context
from models import *
from flask_cors import CORS, cross_origin
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, create_refresh_token, get_jwt_identity
from sqlalchemy import and_, or_, String, cast
from datetime import timedelta
from service.FileUtil import *
from service.LinkMaker import *
from fpdf import FPDF
from pdfrw import PageMerge, PdfReader, PdfWriter

# from werkzeug.security import check_password_hash, generate_password_hash

import base64
import json
import config
import database
import ssl
import re

# context = ssl.SSLContext()
# context.load_cert_chain('cert.pem', 'key.pem')

import typing  
from borb.pdf.document import Document  
from borb.pdf.pdf import PDF  
from borb.toolkit.text.regular_expression_text_extraction import RegularExpressionTextExtraction
from PyPDF4 import PdfFileWriter, PdfFileReader
import sys
import math


def create_app():
  flask_app = Flask(__name__)
  print(config.DATABASE_CONNECTION_URI)
  flask_app.config['SQLALCHEMY_DATABASE_URI'] = config.DATABASE_CONNECTION_URI
  flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
  flask_app.config['JWT_SECRET_KEY'] = config.JWT_SECRET_KEY
  flask_app.config['UPLOAD_FOLDER_PROOF'] = config.UPLOAD_FOLDER_PROOF
  flask_app.config['UPLOAD_FOLDER_REPORT'] = config.UPLOAD_FOLDER_REPORT
  flask_app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(hours=12)
  flask_app.config["JWT_REFRESH_TOKEN_EXPIRES"] = timedelta(days=30)
  flask_app.app_context().push()
  db.init_app(flask_app)
  db.create_all()
  return flask_app

app = create_app()
JWTManager(app)
cors = CORS(app, resources={r'/*': {'origins': '*'}})
# resources = {r"/api/*": {"origins": "*"}}
# app.config["CORS_HEADERS"] = "Content-Type"
# app.config['JSON_SORT_KEYS'] = False

@app.route('/')
def home():
    return jsonify({"Message":"Hello Khoa! This is your flask app with docker"})



@app.route('/v1/login', methods=['POST'])
@cross_origin()
def login():
  try:
    email = request.json.get('email', '')
    matkhau = request.json.get('password', '')
    nd = NguoiDung.query.filter_by(email=email).first()

    if nd:
      # is_pass_correct = check_password_hash(user.password, password)
      is_pass_correct = nd.matkhau == matkhau

      if is_pass_correct:
        access = create_access_token(identity={"email": nd.email, "hoten": nd.hoten, "role": nd.loaind}, fresh=True)
        refresh = create_refresh_token(identity={"email": nd.email, "hoten": nd.hoten, "role": nd.loaind})

        return jsonify({
          'user': {
                      'access': access,
                      'refresh': refresh
                  }}), 200
      else:
        return jsonify({'error': 'Wrong credentials'}), 401
  except Exception as e:
    return str(e), False


@app.route('/v1/me', methods=['GET'])
@jwt_required()
def me():
    req = get_jwt_identity()
    nd = NguoiDung.query.filter_by(email=req.get('email')).first()

    return jsonify({
        'hoten': nd.hoten,
        'email': nd.email,
        'role': nd.loaind
    }), 200


@app.route('/proof/search', methods=['POST'])
@jwt_required()
def searchProof():
  req = request.get_json()
  # exp1, exp2 = [], []
  # if req.get('tendv'):
  #   exp1 = [DonVi.tendv.ilike("%{}%".format(req.get('tendv')))] 
  #   del req['tendv']
  # if req.get('tenloai'):
  #   exp2 = [LoaiMinhChung.tenloai.ilike("%{}%".format(req.get('tenloai')))]
  #   del req['tenloai']
  cons = []
  for col, val in req.items():
    if (req[col] is None) or str(req[col]).strip()=="":
      continue
    elif (col=='mamc'):
      cons.append(cast(getattr(MinhChung, col), String) == "{}".format(val))
    else:
      cons.append(cast(getattr(MinhChung, col), String).ilike("%{}%".format(val)))

  # rs = db.session.query(MinhChung, DonVi, LoaiMinhChung).join(DonVi).filter(*exp1).join(LoaiMinhChung).filter(*exp2).\
  #                   filter(and_(*cons)).order_by(MinhChung.mamc).all()
  rs = MinhChung.query.filter(and_(True, *cons)).order_by(MinhChung.mamc).all()
  # rs = [it[0].as_dict() | it[1].as_dict() | it[2].as_dict() for it in rs]
  rs = [it.as_dict() for it in rs]
  return jsonify(rs)


@app.route('/proof/add', methods=['POST'])
@jwt_required()
def addProof():
  kwargs = json.loads(request.form.to_dict(flat=False).get("info")[0])
  instance = MinhChung(**kwargs)
  try:
      db.session.add(instance)
      db.session.commit()
      resp = FileUtil().upload(0, instance.as_dict().get("mamc"))
      if resp.status_code!=201:
        raise Exception(resp)
  except Exception:
      db.session.rollback()
      return "Fail to create!", False
  else:
      return instance.as_dict(), True


@app.route('/proof/update', methods=['POST'])
@jwt_required()
def updateProof():
  kwargs = json.loads(request.form.to_dict(flat=False).get("info")[0])
  try:
      instance = MinhChung.query.filter_by(mamc=kwargs.get('mamc', ''))
      instance.update(kwargs)
      db.session.commit()
      if not instance.first():
        raise Exception("Proof not found!")
      resp = FileUtil().upload(0, instance.first().as_dict().get("mamc"))
      # if resp.status_code!=201:
      #   raise Exception(resp)
  except Exception as e:
      db.session.rollback()
      return str(e), False
  else:
      return instance.first().as_dict(), True


@app.route('/proof/delete', methods=['POST'])
@jwt_required()
def deleteProof():
  kwargs = request.get_json() or {}
  try:
      instance = MinhChung.query.filter_by(**kwargs)
      if not instance:
        raise Exception("No found")
      instance.delete()
      db.session.commit()

  except Exception:
      db.session.rollback()
      return "No found", False
  else:
      return "Success", True


@app.route('/proof/type', methods=['GET'])
@jwt_required()
def getTypes():
  rs = [it.as_dict() for it in LoaiMinhChung.query.all()];
  return jsonify(rs)


@app.route('/department/all', methods=['GET'])
@jwt_required()
def getDepartments():
  rs = [it.as_dict() for it in DonVi.query.all()];
  return jsonify(rs)

@app.route('/department/search', methods=['POST'])
@jwt_required()
def searchDepart():
  req = request.get_json()
  for col, val in req.items():
    rs = DonVi.query.filter(or_(*[getattr(DonVi, col).ilike("%{}%".format(val)) for col, val in req.items()])).all()
  rs = [it.as_dict() for it in rs]
  return jsonify(rs)


@app.route('/department/add', methods=['POST'])
@jwt_required()
def addDepart():
  kwargs = request.get_json() or {}
  instance = DonVi(**kwargs)
  try:
      db.session.add(instance)
      db.session.commit()
  except Exception:
      db.session.rollback()
      instance = DonVi.query.filter_by(**kwargs).one()
      return instance.as_dict(), False
  else:
      return instance.as_dict(), True


@app.route('/department/update', methods=['POST'])
@jwt_required()
def updateDepart():
  kwargs = request.get_json() or {}
  try:
      instance = DonVi.query.filter_by(madv=kwargs.get('madv', ''))
      instance.update(kwargs)
      db.session.commit()

  except Exception:
      db.session.rollback()
      return instance, False
  else:
      return instance.first().as_dict(), True


@app.route('/department/delete', methods=['POST'])
@jwt_required()
def deleteDepart():
  kwargs = request.get_json() or {}
  try:
      instance = DonVi.query.filter_by(madv=kwargs.get('madv', ''))
      instance.delete()
      db.session.commit()

  except Exception:
      db.session.rollback()
      return instance, False
  else:
      return instance.first().as_dict(), True


@app.route('/usertype/add', methods=['POST'])
@jwt_required()
def addUserType():
  kwargs = request.get_json() or {}
  instance = LoaiNguoiDung(**kwargs)
  try:
      db.session.add(instance)
      db.session.commit()
  except Exception:
      db.session.rollback()
      instance = LoaiNguoiDung.query.filter_by(**kwargs).one()
      return instance.as_dict(), False
  else:
      return instance.as_dict(), True


@app.route('/usertype/delete', methods=['POST'])
@jwt_required()
def deleteUserType():
  kwargs = request.get_json() or {}
  try:
      instance = LoaiNguoiDung.query.filter_by(maloai=kwargs.get('maloai', ''))
      if not instance:
        raise Exception("No found")
      instance.delete()
      db.session.commit()

  except Exception:
      db.session.rollback()
      return "No found", False
  else:
      return "Success", True


@app.route('/usertype/update', methods=['POST'])
@jwt_required()
def updateUserType():
  kwargs = request.get_json() or {}
  try:
      instance = LoaiNguoiDung.query.filter_by(maloai=kwargs.get('maloai', ''))
      instance.update(kwargs)
      db.session.commit()

  except Exception:
      db.session.rollback()
      return instance, False
  else:
      return "Success", True


@app.route('/proof/download/<id>', methods=['GET'])
@jwt_required()
def downloadFile(id):
  try:
    return send_file("."+app.config['UPLOAD_FOLDER_PROOF']+id+".pdf", as_attachment=True)
  except FileNotFoundError:
    return "File not found", False


@app.route('/proof/pull/<id>', methods=['GET'])
@jwt_required()
def pullFile(id):
    # path = app.config['UPLOAD_FOLDER_PROOF']+id+".pdf"
    # with open(path, "rb") as pdf_file:
    #   encoded_string = base64.b64encode(pdf_file.read())
    #   return encoded_string;
  ids = id.split('&')
  return FileUtil().mergeProof(ids)


@app.route('/report/download/<id>', methods=['GET'])
@jwt_required()
def downloadReport(id):
  try:
    path = app.config['UPLOAD_FOLDER_REPORT']+id+".pdf"
    with open(path, "rb") as pdf_file:
      encoded_string = base64.b64encode(pdf_file.read())
      return encoded_string;
  except FileNotFoundError:
    return "File not found", False


@app.route('/feedback', methods=['GET', 'POST', 'DELETE'])
@jwt_required()
def feedback():
  if request.method == 'GET':
    rs = [it.as_dict() for it in YeuCau.query.filter_by(**request.args.to_dict()).all()];
    return jsonify(rs)

  elif request.method == 'POST':
    kwargs = request.get_json() or {}
    instance = YeuCau(**kwargs)
    try:
        db.session.add(instance)
        db.session.commit()
    except Exception:
        db.session.rollback()
        return "Fail to create", False
    else:
        return instance.as_dict(), True

  elif request.method == 'DELETE':
    try:
        instance = YeuCau.query.filter_by(**request.args.to_dict())
        if not instance:
          raise Exception("No found")
        instance.delete()
        db.session.commit()
    except Exception:
        db.session.rollback()
        return "No found to delete", False
    else:
        return "Success", True


@app.route('/report', methods=['GET', 'POST', 'PUT', 'DELETE'])
@jwt_required()
def report():
  if request.method == 'GET':
    req = request.args.to_dict()
    rs = db.session.query(BaoCao, DonVi).join(DonVi).\
                    filter(and_(True, *[cast(getattr(MinhChung, col), String).ilike("%{}%".format(val))\
                                                               for col, val in req.items()]))
    rs = [it[0].as_dict() | it[1].as_dict() for it in rs]
    return jsonify(rs)

  elif request.method == 'POST':
    kwargs = json.loads(request.form.to_dict(flat=False).get("info")[0])
    instance = BaoCao(**kwargs)
    try:
        db.session.add(instance)
        db.session.commit()
        reportID = instance.as_dict().get("id")
        resp = FileUtil().upload(1, reportID)
        if resp.status_code!=201:
          raise Exception(resp)

        @after_this_request
        def add_close_action(response):
          @response.call_on_close
          @copy_current_request_context
          def tagLink():
            LinkMaker().tagLink2(reportID)
            print('Add hyperlink for report {} successfully!'.format(reportID))
          return response

    except Exception as e:
        db.session.rollback()
        return str(e), False
    else:
        return instance.as_dict(), True

  elif request.method == 'PUT': #just update attached file
    try:
      kwargs = json.loads(request.form.to_dict(flat=False).get("info")[0])
      report = BaoCao.query.filter_by(id = kwargs.get("id")).first()
      if (report):
        resp = FileUtil().upload(1, str(report.id))
        if resp.status_code!=201:
          raise Exception(resp)
        @after_this_request
        def add_close_action(response):
          @response.call_on_close
          @copy_current_request_context
          def tagLink():
            LinkMaker().tagLink2(report.id)
            print('Add hyperlink for report {} successfully!'.format(report.id))
          return response
    except Exception as e:
      return str(e), False
    else:
      return report.as_dict(), True

  elif request.method == 'DELETE':
    try:
        instance = BaoCao.query.filter_by(**request.args.to_dict())
        if not instance:
          raise Exception("No found")
        instance.delete()
        db.session.commit()
    except Exception:
        db.session.rollback()
        return "No found to delete", False
    else:
        return "Success", True


@app.route('/enproof', methods=['GET', 'POST', 'DELETE'])
@jwt_required()
@cross_origin()
def enproof():
  if request.method == 'GET':
    req = request.args.to_dict()
    rs = db.session.query(MaHoaMinhChung, MinhChung).filter(MaHoaMinhChung.mamc == MinhChung.mamc).\
                    filter(and_(True, *[cast(getattr(MaHoaMinhChung, col), String)=="{}".format(val)\
                                                               for col, val in req.items()])).all()
    rs = [it[0].as_dict() | it[1].as_dict() for it in rs]
    return jsonify(rs)
  elif request.method == 'POST':
    kwargs = request.get_json()
    proofList = kwargs.get('mamc', '')
    rs = []
    try:
      for proof in proofList.split(','):
        kwargs['mamc'] = proof
        instance = MaHoaMinhChung(**kwargs)
        rs.append(instance.as_dict())
        db.session.add(instance)
      db.session.commit()
    except Exception as e:
      db.session.rollback()
      return str(e), False
    else:
      return jsonify(rs), True
  elif request.method == 'DELETE':
    try:
        instance = MaHoaMinhChung.query.filter_by(**request.args.to_dict()) #delete based on idmh, madv, nam
        if not instance:
          raise Exception("No found")
        instance.delete()
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        return str(e), False
    else:
        return "Success", True


@app.route('/tieuchi', methods=['GET', 'POST', 'DELETE'])
@jwt_required()
@cross_origin()
def tieuchi():
  if request.method == 'GET':
    req = request.args.to_dict()
    rs = [it.as_dict() for it in TieuChi.query.filter_by(**request.args.to_dict()).all()];
    return jsonify(rs)

  elif request.method == 'POST':
    kwargs = request.get_json()
    instance = TieuChi(**kwargs)
    try:
        db.session.add(instance)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        return str(e), False
    else:
        return instance.as_dict(), True

  elif request.method == 'DELETE':
    try:
        instance = TieuChi.query.filter_by(**request.args.to_dict())
        if not instance:
          raise Exception("No found")
        instance.delete()
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        return str(e), False
    else:
        return "Success", True


@app.route('/tieuchuan', methods=['GET', 'POST', 'DELETE'])
@jwt_required()
@cross_origin()
def tieuchuan():
  if request.method == 'GET':
    req = request.args.to_dict()
    rs = [it.as_dict() for it in TieuChuan.query.filter_by(**request.args.to_dict()).all()];
    return jsonify(rs)

  elif request.method == 'POST':
    kwargs = request.get_json()
    instance = TieuChuan(**kwargs)
    try:
        db.session.add(instance)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        return str(e), False
    else:
        return instance.as_dict(), True

  elif request.method == 'DELETE':
    try:
        instance = TieuChuan.query.filter_by(**request.args.to_dict())
        if not instance:
          raise Exception("No found")
        instance.delete()
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        return str(e), False
    else:
        return "Success", True


if __name__ == "__main__":
    # app.run(debug = True, host='0.0.0.0', port=443,  ssl_context=context)
    app.run(debug = True, host='0.0.0.0', port=7007)
