FROM python:3.9.0-alpine
WORKDIR /code

RUN apk add build-base

RUN apk --update --upgrade add --no-cache  gcc musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev 

RUN python -m pip install --upgrade pip

COPY requirements.txt requirements.txt

RUN apk add --no-cache --virtual .build-deps gcc gdb libc-dev libxslt-dev && \
    apk add --no-cache libxslt && \
    apk add --no-cache postgresql-dev && \
    pip install --no-cache-dir lxml>=3.5.0 && \
    apk del .build-deps

RUN pip install -r requirements.txt

RUN apk add gcc g++ cmake make mupdf-dev freetype-dev
ARG MUPDF=1.18.0
RUN ln -s /usr/include/freetype2/ft2build.h /usr/include/ft2build.h \
    && ln -s /usr/include/freetype2/freetype/ /usr/include/freetype \
    && wget -c -q https://www.mupdf.com/downloads/archive/mupdf-${MUPDF}-source.tar.gz \
    && tar xf mupdf-${MUPDF}-source.tar.gz \
    && cd mupdf-${MUPDF}-source \
    && make HAVE_X11=no HAVE_GLUT=no shared=yes prefix=/usr/local install \
    && cd .. \
    && rm -rf *.tar.gz mupdf-${MUPDF}-source
RUN pip install PyMuPDF==1.18.9

EXPOSE 7007
COPY . .
CMD [ "python", "webapp/app.py" ]
